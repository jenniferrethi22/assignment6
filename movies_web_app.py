from __future__ import print_function
import os
import time

import flask
import mysql.connector
from mysql.connector import errorcode
from flask import request



application = flask.Flask(__name__)
app = application



# db = os.environ.get("RDS_DB_NAME", None)
# username = os.environ.get("RDS_USERNAME", None)
# password = os.environ.get("RDS_PASSWORD", None)
# hostname = os.environ.get("RDS_HOSTNAME", None)

def get_db_creds():
    # db = "moviesDB" #os.environ.get("RDS_DB_NAME", None)
    # username = "root" #os.environ.get("RDS_USERNAME", None)
    # password = "fartsareyummy22" #os.environ.get("RDS_PASSWORD", None)
    # hostname = "127.0.0.1" #os.environ.get("RDS_HOSTNAME", None)
    db = os.environ.get("DB") or os.environ.get("database")
    username = os.environ.get("USER") or os.environ.get("username")
    password = os.environ.get("PASSWORD") or os.environ.get("password")
    hostname = os.environ.get("HOST") or os.environ.get("dbhost")
    return db, username, password, hostname


# function to create a table in the database
def create_table():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    # Check if table exists or not. Create and populate it only if it does not exist.
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, title VARCHAR(255), year INT(4), \
    director VARCHAR(255), actor VARCHAR(255), release_date VARCHAR(255), rating FLOAT, PRIMARY KEY (id), UNIQUE(title))'
    try:
        cur = cnx.cursor()
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

@app.route("/")
def index() :
    entries = create_table()
    return flask.render_template('index.html', entries=entries)



@app.route("/add_movie", methods=['POST'])
def insert_movie() :
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    title = request.form['title'].lower()
    year = request.form['year'].lower()
    actor = request.form['actor'].lower()
    director = request.form['director'].lower()
    release_date = request.form['release_date'].lower()
    rating = float(request.form['rating'])
    movie_data = {
        "title": title,
        "year": year,
        "actor": actor,
        "director": director,
        "release_date": release_date,
        "rating": rating,
    }
    add_movie = ("INSERT INTO movies " \
    "(title, year, director, actor, release_date, rating) " \
    "VALUES (%(title)s, %(year)s, %(director)s, %(actor)s, %(release_date)s, %(rating)s)")
    errmsg= None
    try:
        cur = cnx.cursor()
        cur.execute(add_movie, movie_data)
        cnx.commit()
        errmsg="Movie %s successfully inserted" % title
    except mysql.connector.Error as err:
        print(err.msg)
        errmsg="Movie %s could not be inserted - %s" % (title, err.msg)

    return flask.render_template('insert.html', errmsg=errmsg)

@app.route("/insert")
def insert() :
    return flask.render_template('insert.html')

@app.route("/update_movie", methods=['POST'])
def update_movie() :
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    title = request.form['title'].lower()
    year = request.form['year'].lower()
    actor = request.form['actor'].lower()
    director = request.form['director'].lower()
    release_date = request.form['release_date'].lower()
    rating = float(request.form['rating'])
    errmsg= None
    try:
        cur = cnx.cursor(buffered=True)
        get_movie = "SELECT * FROM movies WHERE title='%s' and year='%s'" % (title, year)
        cur.execute(get_movie)
        cnx.commit()
        #movies = [row for row in cur.fetchall()]
        if cur.rowcount == 0:
            errmsg="Movie %s could not be updated, does not exisit" % title
            return flask.render_template('update.html', errmsg=errmsg)
        else:
            cur = cnx.cursor()
            cur.execute ("""
               UPDATE movies
               SET title=%s, year=%s, actor=%s, director=%s, release_date=%s, rating=%s
               WHERE title=%s
            """, (title, year, actor, director, release_date, rating, title))
            cnx.commit()
            errmsg="Movie %s successfully updated" % title
    except mysql.connector.Error as err:
            print(err.msg)
            errmsg="Movie %s could not be updated, %s" % (title, err.msg)
    return flask.render_template('update.html', errmsg=errmsg)

@app.route("/update")
def update():
    return flask.render_template('update.html')

@app.route("/delete_movie", methods=['POST'])
def delete_movie() :
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    title = request.form['delete_title'].lower()
    errmsg=None
    try:
        cur = cnx.cursor(buffered=True)
        get_movie = "SELECT * FROM movies WHERE title='%s'" % title
        cur.execute(get_movie)
        cnx.commit()
        #movies = [row for row in cur.fetchall()]
        if cur.rowcount == 0:
            errmsg="Movie %s could not be deleted, does not exisit" % title
            return flask.render_template('delete.html', errmsg=errmsg)
        cur = cnx.cursor()
        delstatmt = "DELETE FROM movies WHERE title='%s'" % title
        cur.execute(delstatmt)
        cnx.commit()
        errmsg="Movie %s successfully deleted" % title
    except mysql.connector.Error as err:
            print(err.msg)
            errmsg="Movie %s could not be deleted, %s" % (title, err.msg)
    return flask.render_template('delete.html', errmsg=errmsg)

@app.route("/delete")
def delete() :
    return flask.render_template('delete.html')

@app.route("/search_movie", methods=['POST'])
def search_movie() :
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    actor = request.form['search_actor'].lower()
    errmsg= None
    try:
        cur = cnx.cursor(buffered=True)
        get_movie = "SELECT * FROM movies WHERE actor='%s'" % actor
        cur.execute(get_movie)
        cnx.commit()
        movies = [tuple(i for i in (row[1], row[2], row[4])) for row in cur.fetchall()]
        if len(movies) == 0:
            errmsg= "No movies found for actor %s" % actor
    except mysql.connector.Error as err:
            print(err.msg)
            errmsg="Could not find actor %s, %s" % (actor, err.msg)
    return flask.render_template('search.html', movies=movies, actor=actor, errmsg=errmsg)

@app.route("/search")
def search():
    return flask.render_template('search.html')

@app.route("/stats")
def print_stats() :
    return flask.render_template('stats.html')

@app.route("/highest_rating", methods=["GET"])
def highest_rating() :
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    movies = []
    try:
        cur = cnx.cursor(buffered=True)
        get_movie = "SELECT * FROM movies WHERE rating=(SELECT MAX(rating) FROM movies)"
        cur.execute(get_movie)
        cnx.commit()
        movies = [tuple(i for i in (row[1], row[2], row[3], row[4], row[5], row[6])) for row in cur.fetchall()]
    except mysql.connector.Error as err:
            print(err.msg)
    return flask.render_template('stats.html', movies=movies)

@app.route("/lowest_rating", methods=["GET"])
def lowest_rating() :
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                  host=hostname,
                                  database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    try:
        cur = cnx.cursor(buffered=True)
        get_movie = "SELECT * FROM movies WHERE rating=(SELECT MIN(rating) FROM movies)"
        cur.execute(get_movie)
        cnx.commit()
        movies = [tuple(i for i in (row[1], row[2], row[3], row[4], row[5], row[6])) for row in cur.fetchall()]
    except mysql.connector.Error as err:
            print(err.msg)
    return flask.render_template('stats.html', movies=movies)

if __name__ == "__main__":
    #app.debug = True
    app.run(host='0.0.0.0')
